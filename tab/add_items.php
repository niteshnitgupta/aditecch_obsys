<?php 
include('dbs.php');
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) 
{
  die("Connection failed: " . $conn->connect_error);
}

$delete_prod_ids = array();
$ordID = null;

$order_all = array();
$check_all = array();

foreach($_REQUEST as $key => $value) {
  if (strpos($key, 'ORD') === 0) {
    $order_all[$key] = $value;
  } elseif (strpos($key, 'CHK') === 0) {
    $check_all[$key] = $value;
  }
}

foreach($order_all as $entry => $qty) {
  if (strpos($entry, 'ORD') !== 0) {
    continue;
  }
  $entry = str_replace("ORD", "", $entry);
  $entry = explode("_",$entry);
  $key_check = "CHK" . $entry[0] . "_" . $entry[1];
  if (!array_key_exists($key_check, $check_all)) {
    continue;
  }
  $ordID = $entry[0];
  $prdID = $entry[1];
  $size =  $entry[2];
  if ($qty == 0 || $qty == "") {
    $delete_prod_ids[] = "(prodSl='" . $prdID . "' AND prodSize='" . $size . "')"; 
  } else {
    $sql = "INSERT INTO mast_order_det (ordSl, prodSl, prodSize, prodQty) VALUES ('" . $ordID . "','" . $prdID . "','" . $size . "','" . $qty . "') ON DUPLICATE KEY UPDATE prodQty='" . $qty . "'";
    //echo $sql . "<br />";
    $result = $conn->query($sql);
  }
}
if (! empty($delete_prod_ids)) {
  $delete_qry = "DELETE FROM `mast_order_det` WHERE ordSl='" . $ordID . "' AND (" . implode(" OR ", $delete_prod_ids) . ")";
  $result = $conn->query($delete_qry);
}
header('Location: order_details.php?id=' . $ordID);
/*
$conn = new mysqli($servername, $username, $password, $dbname);
                       					 // Check connection
                        				if ($conn->connect_error) 
                        				{
                        				die("Connection failed: " . $conn->connect_error);
                        				}
                  $sql = "SELECT distinct(prodSeg1) as prodSeg1 FROM `mast_product`";
                  $result = $conn->query($sql);
                  
                  while($row = $result->fetch_assoc()) 
                  {
                  }
*/                
?>