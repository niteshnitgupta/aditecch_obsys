<?php 
include('dbs.php');
function mail_attachment($filename, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {

  $file = $filename;
    $file_size = filesize($file);
    $handle = fopen($file, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));
    $name = basename($file);
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$replyto."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"".$uid."\"\r\n\r\n";
    $nmessage = "--".$uid."\r\n";
    $nmessage .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $nmessage .= $message."\r\n\r\n";
    $nmessage .= "--".$uid."\r\n";
    $nmessage .= "Content-Type: application/octet-stream; name=\"".$filename."\"\r\n";
    $nmessage .= "Content-Transfer-Encoding: base64\r\n";
    $nmessage .= "Content-Disposition: attachment; filename=\"".$filename."\"\r\n\r\n";
    $nmessage .= $content."\r\n\r\n";
    $nmessage .= "--".$uid."--";
  if (mail($mailto, $subject, $nmessage, $header)) {
    echo "mail send ... OK"; // or use booleans here
  } else {
    echo "mail send ... ERROR!";
  }
 }
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) 
{
  die("Connection failed: " . $conn->connect_error);
}

$sql = "UPDATE `mast_order` SET `ordStatus` = 'checkout' WHERE `mast_order`.`ordSl` = " . $_REQUEST['id'] . ";";
$result = $conn->query($sql);

require_once dirname(__FILE__) . '/PHP_Excel/PHPExcel.php';
$objPHPExcel = new PHPExcel();

$array_excel = array();
$array_excel[] = array('Order Time Stamp','Created By User','Customer Name','SORT NO','COLOUR','SLEEVS','PROVISION','28','30','32','34','36','38','39','40','42','44','46');

$order_date_time = "DATE";
$order_user = "USER";
$order_cust = "CUST";
$order_remarks = "REMARKS";
$order_email = "EMAIL";

$sql = "SELECT * FROM `mast_order` LEFT JOIN mast_salesman ON mast_salesman.smSl = mast_order.smSl WHERE ordSl='" . $_REQUEST['id'] . "'";
$result = $conn->query($sql);
if ($row = $result->fetch_assoc()) {
  $order_date_time = $row['dtstamp'];
  $order_user = $row['smName'];
  $order_cust = $row['ordCustName'];
  $order_remarks = $row['ordRem'];
  $order_email = $row['ordCustEmail'];
}

$sql = "SELECT `prodSize`,`prodQty`,`autoSl`,`ordSl`,`mast_order_det`.`prodSl` FROM `mast_product` JOIN `mast_order_det` ON `mast_product`.`prodSl`=`mast_order_det`.`prodSl` WHERE ordSl='" . $_REQUEST['id'] . "'";
$result = $conn->query($sql);
$data_order = array();
$prod_order_id_list = array();
$data = array();
while($row = $result->fetch_assoc()) {
  $data_order[] = $row;
  $prod_order_id_list[] = $row['prodSl'];
}
if (!empty($prod_order_id_list)) {
  $prod_order_id_list = array_unique($prod_order_id_list);
  $sql = "SELECT * FROM `mast_product` WHERE prodSl in ('" . implode("','",$prod_order_id_list) . "')"; 
  $result = $conn->query($sql);
  while($row = $result->fetch_assoc()) {
    $orders = array();
    foreach ($data_order as $order) {
      if ($order["prodSl"] == $row["prodSl"]) {
        $orders[] = $order;
      }
    }
    $row["orders"] = $orders; 
    $data[] = $row;
  }
}

foreach ($data as $item) {
  $item_sort = explode("-", $item['prodSeg2'])[0];
  $item_color = explode("-", $item['prodSeg2'])[1];
  $item_sleevs = $item['prodSeg5'];
  $item_provision = $item['prodSeg6'];
  $item_28 = 0;
  $item_30 = 0;
  $item_32 = 0;
  $item_34 = 0;
  $item_36 = 0;
  $item_38 = 0;
  $item_39 = 0;
  $item_40 = 0;
  $item_42 = 0;
  $item_44 = 0;
  $item_46 = 0;
  foreach ($item['orders'] as $item_order) {
    if ($item_order['prodSize'] == "28") {
      $item_28 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "30") {
      $item_30 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "32") {
      $item_32 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "34") {
      $item_34 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "36") {
      $item_36 = $item_order['prodQty'];
    } else if ($item_order['prodSize'] == "38") {
      $item_38 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "39") {
      $item_39 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "40") {
      $item_40 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "42") {
      $item_42 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "44") {
      $item_44 = $item_order['prodQty'];
    } elseif ($item_order['prodSize'] == "46") {
      $item_46 = $item_order['prodQty'];
    }
  }
  $array_excel[] = array($order_date_time, $order_user, $order_cust, $item_sort, $item_color, $item_sleevs, $item_provision, $item_28, $item_30, $item_32, $item_34, $item_36, $item_38, $item_39, $item_40, $item_42, $item_44, $item_46);
}

$objPHPExcel->getActiveSheet()->fromArray($array_excel, NULL, 'A1');

$objPHPExcel->getActiveSheet()->setTitle('Simple');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('excel.xls');

$order_mail_office = "Order No: " . $_REQUEST['id'] . "\nCustomer Name: " . $order_cust . "\nOrder Remarks: " . $order_remarks . "\nOrder Creation DateTime: " . $order_date_time . "\n";
$order_mail_cust = "Thank you for placing the order. Order details can be found as mention in attached excel file.\n\nOur sales team will co-ordinate with you regarding the order status.\n\nOrder No: " . $_REQUEST['id'] . "\nOrder Creation DateTime: " . $order_date_time . "\n";

mail_attachment('excel.xls', 'nitesh.nit.gupta@gmail.com,avishek@aditechsolutions.com', 'info@aditechsolutions.com', 'OBS - Sale Order', 'info@aditechsolutions.com', 'OBS Sale Order', $order_mail_office);
mail_attachment('excel.xls', 'nitesh.nit.gupta@gmail.com,avishek@aditechsolutions.com', 'info@aditechsolutions.com', 'OBS - Sale Order', 'info@aditechsolutions.com', 'OBS Sale Order', $order_mail_cust);
?>
