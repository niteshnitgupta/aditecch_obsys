<?php 
include('dbs.php');
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
                       					 // Check connection
                        				if ($conn->connect_error) 
                        				{
                        				die("Connection failed: " . $conn->connect_error);
                        				}
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>OBS</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

    <!-- - var navbarShadow = true-->
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-light bg-gradient-x-grey-blue">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.php" class="navbar-brand"><img alt="stack admin logo" src="app-assets/images/logo/stack-logo.png" class="brand-logo">
                <h2 class="brand-text">OBS</h2></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" navigation-header"><span>Menu</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
          </li>
         
         <li class=" nav-item"><a href="index.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Home</span></a>            
          </li>
          
          <li class=" nav-item"><a href="products.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Products</span></a>            
          </li> 

          <li class=" nav-item"><a href="orders.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Orders</span></a>            
          </li>  
         
        </ul>
        
      </div>
    </div>

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          
          
        </div>
        <div class="content-body"><!-- Input Mask start -->
<section class="inputmask" id="inputmask">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Dashboard</h4>-->
					
        			
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
							
							
							
							
							
							
							
							<section id="child-row-control">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Manage Order</h4>-->
					
					
					<!--<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							
						</ul>
					</div>-->
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
					
					
					<fieldset class="mb-1">
									<center><h5>Add Product to Order</h5></center>
									<!--<p class="text-muted"> if <code>alwaysShow: true</code> the threshold will be ignored and the remaining length indication will be always showing up while typing or on focus on the input.</p>-->
									
									
					
					<hr/>
          <div id="searchbar">
					<table class="table table-striped table-bordered compact">
            <tr>
              <td>
                <select class="form-control" id="prod_segment1">
                <?php
                  $sql = "SELECT distinct(prodSeg1) as prodSeg1 FROM `mast_product`";
                  $result = $conn->query($sql);
                  
                  while($row = $result->fetch_assoc()) 
                  {
                ?>
                  <option><?php echo $row['prodSeg1']; ?></option>
                <?php	
                  }
                ?>
                </select>
              </td>
              <td>
                <input type="text" name="prod_segment2" id="prod_segment2" class="form-control" />
              </td>
            </tr>
          </table>
					</div>
          <hr/>
					
        <form action="add_items.php" method="post">
          <div id="items">
            
          </div>
					<center>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-check-square-o"></i> ADD TO ORDER
            </button>
          </center>
        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ Whole row child row control table -->
							
								 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Input Mask end -->




        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-dark navbar-border">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Demo - Aditech Solutions </span><span class="float-md-right d-xs-block d-md-inline-block hidden-md-down"></span></p>
    </footer>

   

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
    
    <script src="app-assets/vendors/js/forms/extended/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/bloodhound.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/handlebars.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables-extensions/datatable-responsive.js" type="text/javascript"></script>
     <script src="app-assets/js/scripts/forms/extended/form-typeahead.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-inputmask.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-maxlength.js" type="text/javascript"></script>

    <script src="app-assets/js/scripts/jquery.blockUI.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script type="text/javascript">
      //$("#items").block({message: null});
      $("#prod_segment1").change(function(){
        $("#prod_segment2").keyup();
      });
      $("#prod_segment2").keyup(function(){
        if ($("#prod_segment2").val().length >= 2) {
          $.get("segment_filter.php?id=<?php echo $_REQUEST['id']; ?>&prodSeg1=" + $("#prod_segment1").val() + "&prodSeg2=" + $("#prod_segment2").val() + "", function(data){
            data_dump = JSON.parse(data);
            data = data_dump.data;
            var div_item = "";
            for (i=0; i<data.length; i++) {
              item = data[i];
              var div_template = "<div class='item'>"+
              "					<p> <h6 class='primary'> <b> Product: &nbsp;  " + item.prodSeg1 + "-" + item.prodSeg2 + "-" + item.prodSeg5  + "-" + item.prodSeg6 + " </b></h6></p>"+
              "					<table class='table table-striped table-bordered compact'>"+
              "							<tbody>";

              size_arr = item.prodSeg4.split(',');
              
              div_template += "<tr>";
              for (size_count = 0; size_count< size_arr.length; size_count++) {
                if(size_count%2 == 0) {
                  div_template += "</tr><tr>";
                }
                text_value = "";
                item.orders.forEach(function(order){
                  if (size_arr[size_count] == order.prodSize) {
                    text_value = order.prodQty;
                  }
                });
                div_template += "<td>Size " + size_arr[size_count] + 
"								<input type='number' class='numberonly form-control' value='" + text_value + "' aria-label='Amount' name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_" + size_arr[size_count] + "'>" +
"								</td>";
              }
              div_template += "</tr>";

              div_template += ""+
              "							</tbody>"+
              "					</table>"+
              "			</div>";
              div_item += div_template;
            }
            $("#items").html(div_item);
            //$("#items").block({message: null});
          });          
        } else {
          $("#items").html("");
        }
      });
      $(document).ready(function(){
        $(".numberonly").focus(function(){$(this).attr("type","number")});
        $(".numberonly").blur(function(){$(this).attr("type","text")});
      })
      /*$("#prod_search").click(function(){
        $("#searchbar").block({message: null});
        $("#items").unblock({message: null});
      });*/
    </script>
  </body>
</html>