<?php 
include('dbs.php');
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>OBS</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/pages/project.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

    <!-- - var navbarShadow = true-->
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-light bg-gradient-x-grey-blue">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.php" class="navbar-brand"><img alt="stack admin logo" src="app-assets/images/logo/stack-logo.png" class="brand-logo">
                <h2 class="brand-text">OBS</h2></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" navigation-header"><span>Menu</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
          </li>
         
         <li class=" nav-item"><a href="index.php"><i class="ft-home"></i><span data-i18n="" class="menu-title">Home</span></a>            
          </li>
          
          <li class=" nav-item"><a href="#"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Master</span><span class="tag tag tag-primary tag-pill float-xs-right mr-2">2</span></a>
          
          <ul class="menu-content">          
          
          <li class=" nav-item"><a href="products.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Products</span></a>            
          </li> 
	  <li class=" nav-item"><a href="salesman.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Salesman</span></a>            
          </li> 
          
          </ul>
          
          </li>  
          
          <li class=" nav-item"><a href="orders.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Orders</span></a>            
          </li> 
         
        </ul>
        
      </div>
    </div>

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          
          
        </div>
        <div class="content-body"><!-- Input Mask start -->
<section class="inputmask" id="inputmask">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					
					
        			
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
							
							
							
							
							
							
							
							<section id="child-row-control">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">Salesman</h4>
					<!--<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>-->
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							
						</ul>
					</div>
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
						
						<table class="table table-striped table-bordered responsive dataex-res-rowcontrol">
							<thead>
								<tr>
									
									<th>S ID</th>
									<th>Name</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Zone</th>
									<th>Status</th>
									
									
								</tr>
							</thead>
							<tbody>
							
							<?php
							
							$conn = new mysqli($servername, $username, $password, $dbname);
                       					 // Check connection
                        				if ($conn->connect_error) 
                        				{
                        				die("Connection failed: " . $conn->connect_error);
                        				} 
                        
            $sql = "SELECT * FROM mast_salesman";
            $result = $conn->query($sql);
            
            if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) 
                        {
                        
                        ?>
                        
                        
								<tr> 
									
					<td><?php echo $row['smSl']; ?></td>
					<td><?php echo $row['smName']; ?></td>
					<td><?php echo $row['smPhone']; ?></td>
					<td><?php echo $row['smEmail']; ?></td>
					<td><?php echo $row['smZone']; ?></td>
					<td><?php echo $row['smStatus']; ?></td>
					
									
								</tr>
								
								
								<?php
								
								}
								
								}
								
								 ?>
								
							</tbody>
							
						</table>				
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ Whole row child row control table -->
							
							
							
							
							
							
							
							
							
							
							
							
							
								 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Input Mask end -->




        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-dark navbar-border">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Demo - Aditech Solutions </span><span class="float-md-right d-xs-block d-md-inline-block hidden-md-down"></span></p>
    </footer>

   

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables-extensions/datatable-responsive.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
  </body>
</html>