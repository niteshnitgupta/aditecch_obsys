<?php 
include('dbs.php');
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) 
{
  die("Connection failed: " . $conn->connect_error);
}

if(!isset($_REQUEST['type'])) {
  $_REQUEST['type'] = "";
}

if ($_REQUEST['type'] == 'order_details') {
  $sql = "SELECT `prodSize`,`prodQty`,`autoSl`,`ordSl`,`mast_order_det`.`prodSl` FROM `mast_product` JOIN `mast_order_det` ON `mast_product`.`prodSl`=`mast_order_det`.`prodSl` WHERE ordSl='" . $_REQUEST['id'] . "'";
} elseif ($_REQUEST['type'] == 'prodBarcodeLegacy') {
  $sql = "SELECT `prodSize`,`prodQty`,`autoSl`,`ordSl`,`mast_order_det`.`prodSl` FROM `mast_product` LEFT JOIN `mast_order_det` ON `mast_product`.`prodSl`=`mast_order_det`.`prodSl` WHERE ordSl='" . $_REQUEST['id'] . "' AND prodBarcodeLegacy='" . $_REQUEST['prodBarcodeLegacy'] . "'";
} else {
  $sql = "SELECT `prodSize`,`prodQty`,`autoSl`,`ordSl`,`mast_order_det`.`prodSl` FROM `mast_product` LEFT JOIN `mast_order_det` ON `mast_product`.`prodSl`=`mast_order_det`.`prodSl` WHERE ordSl='" . $_REQUEST['id'] . "' AND prodSeg1='" . $_REQUEST['prodSeg1'] . "' AND prodSeg2 like '%" . $_REQUEST['prodSeg2'] . "%'";
}
$result = $conn->query($sql);
$data_order = array();
$prod_order_id_list = array();
$data = array();
while($row = $result->fetch_assoc()) 
{
  $data_order[] = $row;
  $prod_order_id_list[] = $row['prodSl'];
}

if ($_REQUEST['type'] == 'order_details') {
  if (empty($prod_order_id_list)) {
    $result = ['data' => $data, 'data_order' => $data_order, 'errors' => [], 'responseCode' => 0000];
    echo json_encode($result);
    exit;
  }
  $prod_order_id_list = array_unique($prod_order_id_list);
  $sql = "SELECT * FROM `mast_product` WHERE prodSl in ('" . implode("','",$prod_order_id_list) . "')"; 
  //echo $sql;
} elseif ($_REQUEST['type'] == 'prodBarcodeLegacy') {
  $sql = "SELECT * FROM `mast_product` WHERE prodBarcodeLegacy='" . $_REQUEST['prodBarcodeLegacy'] . "'"; 
  //echo $sql;
} else {
  $sql = "SELECT * FROM `mast_product` WHERE prodSeg1='" . $_REQUEST['prodSeg1'] . "' AND prodSeg2 like '%" . $_REQUEST['prodSeg2'] . "%'";
}
  $result = $conn->query($sql);

  while($row = $result->fetch_assoc()) 
  {
    $orders = array();
    foreach ($data_order as $order) {
      if ($order["prodSl"] == $row["prodSl"]) {
        $orders[] = $order;
      }
    }
    $row["orders"] = $orders; 
    $data[] = $row;
  }  

$result = ['data' => $data, 'data_order' => $data_order, 'errors' => [], 'responseCode' => 0000];
echo json_encode($result);
?>