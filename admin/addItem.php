<?php 
include('dbs.php');
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
                       					 // Check connection
                        				if ($conn->connect_error) 
                        				{
                        				die("Connection failed: " . $conn->connect_error);
                        				}
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>OBS</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

    <!-- - var navbarShadow = true-->
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-light bg-gradient-x-grey-blue">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.php" class="navbar-brand"><img alt="stack admin logo" src="app-assets/images/logo/stack-logo.png" class="brand-logo">
                <h2 class="brand-text">OBS</h2></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" navigation-header"><span>Menu</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
          </li>
         
        <li class=" nav-item"><a href="index.php"><i class="ft-home"></i><span data-i18n="" class="menu-title">Home</span></a>            
          </li>
          
          <li class=" nav-item"><a href="#"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Master</span><span class="tag tag tag-primary tag-pill float-xs-right mr-2">2</span></a>
          
          <ul class="menu-content">          
          
          <li class=" nav-item"><a href="products.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Products</span></a>            
          </li> 
	  <li class=" nav-item"><a href="salesman.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Salesman</span></a>            
          </li> 
          
          </ul>
          
          </li>  
          
          <li class=" nav-item"><a href="orders.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Orders</span></a>            
          </li> 
         
        </ul>
        
      </div>
    </div>

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          
          
        </div>
        <div class="content-body"><!-- Input Mask start -->
<section class="inputmask" id="inputmask">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Dashboard</h4>-->
					
        			
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
							
							
							
							
							
							
							
							<section id="child-row-control">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Manage Order</h4>-->
					
					
					<!--<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							
						</ul>
					</div>-->
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
					
					
					<fieldset class="mb-1">
									<center><h5>Add Product to Order</h5></center>
									<!--<p class="text-muted"> if <code>alwaysShow: true</code> the threshold will be ignored and the remaining length indication will be always showing up while typing or on focus on the input.</p>-->
									
									
					
					<hr/>
          <div id="searchbar">
					<table class="table table-striped table-bordered compact">
            <tr>
              <td>
                <select class="form-control" id="prod_segment1">
                <?php
                  $sql = "SELECT distinct(prodSeg1) as prodSeg1 FROM `mast_product`";
                  $result = $conn->query($sql);
                  
                  while($row = $result->fetch_assoc()) 
                  {
                ?>
                  <option><?php echo $row['prodSeg1']; ?></option>
                <?php	
                  }
                ?>
                </select>
              </td>
              <td>
                <input type="text" name="prod_segment2" id="prod_segment2" class="form-control" />
              </td>
            </tr>
          </table>
					</div>
          <hr/>
					
        <form action="add_items.php" method="post">
            <table border=1 width=100%>
              <thead>
                  <tr>
                    <th>Item Name</th>
                    <th>28</th>
                    <th>30</th>
                    <th>32</th>
                    <th>34</th>
                    <th>36</th>
                    <th>38</th>
                    <th>39</th>
                    <th>40</th>
                    <th>42</th>
                    <th>44</th>
                    <th>46</th>
                  </tr>
                </thead>
                <tbody id="items">
                </tbody>
            </table>
          
					<center> <br/>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-save"></i> &nbsp; SAVE ORDER
            </button>
          </center>
        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ Whole row child row control table -->
							
								 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Input Mask end -->




        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-dark navbar-border">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Demo - Aditech Solutions </span><span class="float-md-right d-xs-block d-md-inline-block hidden-md-down"></span></p>
    </footer>

   

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
    
    <script src="app-assets/vendors/js/forms/extended/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/bloodhound.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/handlebars.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables-extensions/datatable-responsive.js" type="text/javascript"></script>
     <script src="app-assets/js/scripts/forms/extended/form-typeahead.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-inputmask.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-maxlength.js" type="text/javascript"></script>

    <script src="app-assets/js/scripts/jquery.blockUI.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    <script type="text/javascript">
      //$("#items").block({message: null});
      var min_prod_qty = 5;
      $("#prod_segment1").change(function(){
        $("#prod_segment2").keyup();
      });
      $("#prod_segment2").keyup(function(){
        if ($("#prod_segment2").val().length >= 2) {
          $.get("segment_filter.php?id=<?php echo $_REQUEST['id']; ?>&prodSeg1=" + $("#prod_segment1").val() + "&prodSeg2=" + $("#prod_segment2").val() + "", function(data){
            data_dump = JSON.parse(data);
            data = data_dump.data;
            var div_item = "";
            for (i=0; i<data.length; i++) {
              item = data[i];
              var size_28 = "";
              var size_30 = "";
              var size_32 = "";
              var size_34 = "";
              var size_36 = "";
              var size_38 = "";
              var size_39 = "";
              var size_40 = "";
              var size_42 = "";
              var size_44 = "";
              var size_46 = "";
              item.orders.forEach(function(order) {
                if (order.prodSize == "28") {
                  size_28 = order.prodQty;
                } else if (order.prodSize == "30") {
                  size_30 = order.prodQty;
                } else if (order.prodSize == "32") {
                  size_32 = order.prodQty;
                } else if (order.prodSize == "34") {
                  size_34 = order.prodQty;
                } else if (order.prodSize == "36") {
                  size_36 = order.prodQty;
                } else if (order.prodSize == "38") {
                  size_38 = order.prodQty;
                } else if (order.prodSize == "39") {
                  size_39 = order.prodQty;
                } else if (order.prodSize == "40") {
                  size_40 = order.prodQty;
                } else if (order.prodSize == "42") {
                  size_42 = order.prodQty;
                } else if (order.prodSize == "44") {
                  size_44 = order.prodQty;
                } else if (order.prodSize == "46") {
                  size_46 = order.prodQty;
                }
              });

              var style_28 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_30 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_32 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_34 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_36 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_38 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_39 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_40 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_42 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_44 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              var style_46 = " style='background-color:#ff8585;width: 3em;' disabled='' ";
              
              size_arr = item.prodSeg4.split(',');

              for (size_count = 0; size_count< size_arr.length; size_count++) {
                if (size_arr[size_count] == "28") {
                  if (item.prodSize28 <= min_prod_qty) {
                    style_28 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_28 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "30") {
                  if (item.prodSize30 <= min_prod_qty) {
                    style_30 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_30 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "32") {
                  if (item.prodSize32 <= min_prod_qty) {
                    style_32 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_32 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "34") {
                  if (item.prodSize34 <= min_prod_qty) {
                    style_34 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_34 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "36") {
                  if (item.prodSize36 <= min_prod_qty) {
                    style_36 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_36 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "38") {
                  if (item.prodSize38 <= min_prod_qty) {
                    style_38 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_38 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "39") {
                  if (item.prodSize39 <= min_prod_qty) {
                    style_39 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_39 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "40") {
                  if (item.prodSize40 <= min_prod_qty) {
                    style_40 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_40 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "42") {
                  if (item.prodSize42 <= min_prod_qty) {
                    style_42 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_42 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "44") {
                  if (item.prodSize44 <= min_prod_qty) {
                    style_44 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_44 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                } else if (size_arr[size_count] == "46") {
                  if (item.prodSize46 <= min_prod_qty) {
                    style_46 = " style='background-color:#FDC95F;width: 3em;' ";
                  } else {
                    style_46 = " style='background-color:#c5ff97;width: 3em;' ";
                  }
                }
              }
              
              var div_template = ""+
"                  <tr>" +
"                    <td style='width:4em;'>" + item.prodSeg1 + "-" + item.prodSeg2 + "-" + item.prodSeg5 + "-" + item.prodSeg6 + "</td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_28 + "' " + style_28 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_28' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_30 + "' " + style_30 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_30' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_32 + "' " + style_32 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_32' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_34 + "' " + style_34 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_34' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_36 + "' " + style_36 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_36' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_38 + "' " + style_38 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_38' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_39 + "' " + style_39 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_39' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_40 + "' " + style_40 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_40' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_42 + "' " + style_42 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_42' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_44 + "' " + style_44 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_44' /></td>" +
"                    <td style='width:3em;'><input class='numberonly' type='number' value='" + size_46 + "' " + style_46 + " class='form-control' size=4 name='ORD<?php echo $_REQUEST['id']; ?>_" + item.prodSl + "_46' /></td>" +
"                  </tr>";
              div_item += div_template;
            }

            $('#items').html(div_item);
            //$("#items").block({message: null});
          });          
        } else {
          $("#items").html("");
        }
      });
      $(document).ready(function(){
        $(".numberonly").focus(function(){$(this).attr("type","number")});
        $(".numberonly").blur(function(){$(this).attr("type","text")});
      })
      /*$("#prod_search").click(function(){
        $("#searchbar").block({message: null});
        $("#items").unblock({message: null});
      });*/
    </script>
  </body>
</html>