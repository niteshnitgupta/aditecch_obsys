<?php 
include('dbs.php');
//echo $barcode;
$conn = new mysqli($servername, $username, $password, $dbname);
                       					 // Check connection
                        				if ($conn->connect_error) 
                        				{
                        				die("Connection failed: " . $conn->connect_error);
                        				}
?>
<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">
    <title>OBS</title>
    <link rel="apple-touch-icon" href="app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i%7COpen+Sans:300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/feather/style.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/extensions/pace.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/colReorder.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/extensions/fixedHeader.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="app-assets/vendors/css/tables/datatable/dataTables.bootstrap4.min.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN STACK CSS-->
    
    <link rel="stylesheet" type="text/css" href="app-assets/css/bootstrap-extended.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/app.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/colors.css">
    <!-- END STACK CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/menu/menu-types/vertical-overlay-menu.css">
    <link rel="stylesheet" type="text/css" href="app-assets/css/core/colors/palette-gradient.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-menu" data-col="2-columns" class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

    <!-- - var navbarShadow = true-->
    <!-- navbar-fixed-top-->
    <nav class="header-navbar navbar navbar-with-menu navbar-fixed-top navbar-semi-light bg-gradient-x-grey-blue">
      <div class="navbar-wrapper">
        <div class="navbar-header">
          <ul class="nav navbar-nav">
            <li class="nav-item mobile-menu hidden-md-up float-xs-left"><a href="#" class="nav-link nav-menu-main menu-toggle hidden-xs"><i class="ft-menu font-large-1"></i></a></li>
            <li class="nav-item"><a href="index.php" class="navbar-brand"><img alt="stack admin logo" src="app-assets/images/logo/stack-logo.png" class="brand-logo">
                <h2 class="brand-text">OBS</h2></a></li>
            <li class="nav-item hidden-md-up float-xs-right"><a data-toggle="collapse" data-target="#navbar-mobile" class="nav-link open-navbar-container"><i class="fa fa-ellipsis-v"></i></a></li>
          </ul>
        </div>
        
      </div>
    </nav>

    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <div data-scroll-to-active="true" class="main-menu menu-fixed menu-light menu-accordion menu-shadow">
      <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">
          <li class=" navigation-header"><span>Menu</span><i data-toggle="tooltip" data-placement="right" data-original-title="General" class=" ft-minus"></i>
          </li>
         
        <li class=" nav-item"><a href="index.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Home</span></a>            
          </li>
          
          <li class=" nav-item"><a href="products.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Products</span></a>            
          </li> 
          
          <li class=" nav-item"><a href="stock.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Stock</span></a>            
          </li> 

          <li class=" nav-item"><a href="orders.php"><i class="ft-layout"></i><span data-i18n="" class="menu-title">Orders</span></a>            
          </li>   
         
        </ul>
        
      </div>
    </div>

    <div class="app-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
          
          
        </div>
        <div class="content-body"><!-- Input Mask start -->
<section class="inputmask" id="inputmask">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Dashboard</h4>-->
					
        			
				</div>
				<div class="card-body collapse in">
					<div class="card-block">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
							
							
							
							
							
							
							
							<section id="child-row-control">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<div class="card-header">
					<!--<h4 class="card-title">Create New Order</h4>-->
					
					
					<!--<a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
					<div class="heading-elements">
						<ul class="list-inline mb-0">
							<li><a data-action="collapse"><i class="ft-minus"></i></a></li>
							
							<li><a data-action="expand"><i class="ft-maximize"></i></a></li>
							
						</ul>
					</div>-->
				</div>
				<div class="card-body collapse in">
					<div class="card-block card-dashboard">
					
					
					<fieldset class="mb-1">
									<center><h5>Create New Order</h5></center>
									
									
									
					
					<hr/>
					
	  <form id="order_new" action="order_add.php" method="POST">
            <div id="searchbar">
					
					
					
					
					<fieldset class="mb-1">
						<h5>Customer Name</h5>
									
					<div class="form-group">
					<input type="text" name="custName" class="form-control always-show-maxlength" id="maxlength-always-show" placeholder="Enter Customer Name" maxlength="20" />
					</div>
					</fieldset>
						
						
					
					
					<fieldset class="mb-1">
						<h5>Customer Email</h5>
									
					<div class="form-group">
					<input type="text" name="email" class="form-control always-show-maxlength" id="maxlength-always-show" placeholder="Customer Email(s) separated by ," maxlength="150" />
					</div>
					</fieldset>
					
						
						
							
								
					<fieldset class="mb-1">
						<h5>Remarks / Desc</h5>
									
					<div class="form-group">
					<input type="text" name="remarks" class="form-control always-show-maxlength" id="maxlength-always-show" placeholder="Enter Customer Name" maxlength="50" />
					</div>
					</fieldset>
					
					
          
					<center> <br/>
            <button type="submit" class="btn btn-primary">
              <i class="fa fa-save"></i> &nbsp; SAVE ORDER
            </button>
          </center>
        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--/ Whole row child row control table -->
							
								 
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- Input Mask end -->




        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->


    <footer class="footer footer-static footer-dark navbar-border">
      <p class="clearfix blue-grey lighten-2 text-sm-center mb-0 px-2"><span class="float-md-left d-xs-block d-md-inline-block">Demo - Aditech Solutions </span><span class="float-md-right d-xs-block d-md-inline-block hidden-md-down"></span></p>
    </footer>

   

    <!-- BEGIN VENDOR JS-->
    <script src="app-assets/vendors/js/vendors.min.js" type="text/javascript"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="app-assets/vendors/js/tables/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/buttons.colVis.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.colReorder.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/tables/datatable/dataTables.fixedHeader.min.js" type="text/javascript"></script>
    
    <script src="app-assets/vendors/js/forms/extended/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/bloodhound.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/typeahead/handlebars.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
    <script src="app-assets/vendors/js/forms/extended/maxlength/bootstrap-maxlength.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN STACK JS-->
    <script src="app-assets/js/core/app-menu.js" type="text/javascript"></script>
    <script src="app-assets/js/core/app.js" type="text/javascript"></script>
    <!-- END STACK JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="app-assets/js/scripts/tables/datatables-extensions/datatable-responsive.js" type="text/javascript"></script>
     <script src="app-assets/js/scripts/forms/extended/form-typeahead.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-inputmask.js" type="text/javascript"></script>
    <script src="app-assets/js/scripts/forms/extended/form-maxlength.js" type="text/javascript"></script>

    <script src="app-assets/js/scripts/jquery.blockUI.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
   
  </body>
</html>